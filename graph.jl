using Graphs, SimpleWeightedGraphs
using WriteVTK
using VTKDataTypes, VTKDataIO
using LinearAlgebra

# INPUT_FILE = "sphere.vtk"
INPUT_FILE = "boolean.vtk"
# INPUT_FILE = "slicer.vtk"
# INPUT_FILE = "square.vtk"

mesh = read_vtk(INPUT_FILE)

# Points
points = [mesh.point_coords[:,i] for i in 1:size(mesh.point_coords,2)]
nv = length(points)

# Trafo
f(p) = p  # no trafo
f(p) = diagm([1,2,1]) * p  # squeeze y-direction
points = f.(points)
mesh.point_coords = hcat(points...)

# Graph
g = SimpleWeightedGraph(nv)
elements = filter(x->length(x)==3, mesh.cell_connectivity)
ne = length(elements)
for el in 1:ne
  # Add edges
  edges = Iterators.product(elements[el], elements[el]) |> collect |> vec
  map(p -> add_edge!(g, p[1], p[2], norm(points[p[1]] - points[p[2]])), edges)
end

# Graph Laplacian
A = laplacian_matrix(g)

# Diagonalization
N = 10
eigobj = eigen(Array(A))
U = eigobj.vectors[:,1:N]
Σ = eigobj.values[1:N]

# Write Output VTK
for i=1:N
  meshtmp = mesh
  meshtmp.point_data["eigvec"] = U[:,i]
  meshtmp.point_data["eigval"] = Σ[i] * ones(length(points))
  write_vtk(mesh, "result_$i.vtu")
end
